/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef RAYHITTABLELIST_H
#define RAYHITTABLELIST_H
#include <vector>

#include "RayHittable.h"

namespace RaygineTracer::Mathematics
{
	class RayHittableList : public RayHittable
	{
	public:
		[[nodiscard]] bool Hit(std::shared_ptr<Ray> ray, const float& tMin, const float& tMax, HitRecord& rec) override;

		[[nodiscard]] std::shared_ptr<AxisAlignedBoundingBox> GetBoundingBox(const float& t0, const float& t1) const override;
		[[nodiscard]] std::vector<std::shared_ptr<RayHittable>> GetRayHittableList() const { return rayHittableList; }

		void AddRayHittable(const std::shared_ptr<RayHittable>& hittable) { rayHittableList.push_back(hittable); }
	private:
		std::vector<std::shared_ptr<RayHittable>> rayHittableList;
		HitRecord hitRecord;
	};
}

#endif // RAYHITTABLELIST_H
