/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef SPHERE_H
#define SPHERE_H
#include "Ray.h"
#include "RayHittable.h"

namespace RaygineTracer::Mathematics
{
	class Sphere : public RayHittable
	{
	public:
		explicit Sphere(const Vector3& _centre, const float& _radius) : centre(_centre), radius(_radius) { }

		[[nodiscard]] bool Hit(std::shared_ptr<Ray>, const float& tMin, const float& tMax, HitRecord& rec) override;

		[[nodiscard]] std::shared_ptr<AxisAlignedBoundingBox> GetBoundingBox(const float& t0, const float& t2) const override;
	private:
		const Vector3 centre;
		const float radius;
	};
}
#endif // SPHERE_H
