/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef RAY_H
#define RAY_H
#include "Vector3.h"

using namespace Common::Mathematics;

namespace TracerTracer::Mathematics
{
	class Ray
	{
	public:
		explicit Ray() = default;
		explicit Ray(const Vector3 &_origin, const Vector3 &_direction) : origin(_origin), direction(_direction) { }

		[[nodiscard]] Vector3 GetOrigin() const { return origin; }
		[[nodiscard]] Vector3 GetDirection() const { return direction; }
		[[nodiscard]] Vector3 PointAt(const float &what) const { return origin + (direction * what); }
	private:
		Vector3 origin;
		Vector3 direction;
	};
}

#endif // RAY_H
