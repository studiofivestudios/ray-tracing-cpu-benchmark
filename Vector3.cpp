/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#include "Vector3.h"

 /**
  * \brief Find the length and return it.
  * \return The length of the vector.
  */
float Common::Mathematics::Vector3::GetLength() const
{
	return std::sqrt((x * x) + (y * y) + (z * z));
}

/**
 * \brief Find the squared length and return it.
 * \return The squared length of the vector.
 */
float Common::Mathematics::Vector3::GetLengthSquared() const
{
	return (x * x) + (y * y) + (z * z);
}

Common::Mathematics::Vector3 Common::Mathematics::Vector3::GetUnitVector(Vector3 vector)
{
	return vector / vector.GetLength();
}

Common::Mathematics::Vector3 Common::Mathematics::Vector3::GetCrossProduct(const Vector3& a, const Vector3& b)
{
	return Vector3((a.y * b.z - a.z * b.y), (-(a.x * b.z - a.z * b.x)), (a.x * b.y - a.y * b.x));
}

float Common::Mathematics::Vector3::DotProduct(const Vector3& a, const Vector3& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}