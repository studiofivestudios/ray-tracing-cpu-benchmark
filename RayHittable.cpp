/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#include "RayHittable.h"

std::shared_ptr<RaygineTracer::Mathematics::AxisAlignedBoundingBox> RaygineTracer::Mathematics::RayHittable::
GetSurroundingBox(std::shared_ptr<AxisAlignedBoundingBox> box0, std::shared_ptr<AxisAlignedBoundingBox> box1) const
{
	Vector3 smallerBox(
		AxisAlignedBoundingBox::FMin(box0->GetMin().x, box1->GetMin().x),
		AxisAlignedBoundingBox::FMin(box0->GetMin().y, box1->GetMin().y),
		AxisAlignedBoundingBox::FMin(box0->GetMin().z, box1->GetMin().z)
	);
	Vector3 largerBox(
		AxisAlignedBoundingBox::FMax(box0->GetMin().x, box1->GetMin().x),
		AxisAlignedBoundingBox::FMax(box0->GetMin().y, box1->GetMin().y),
		AxisAlignedBoundingBox::FMax(box0->GetMin().z, box1->GetMin().z)
	);

	return std::make_shared<AxisAlignedBoundingBox>(smallerBox, largerBox);
}
