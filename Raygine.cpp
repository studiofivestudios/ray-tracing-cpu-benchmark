/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

// ReSharper disable CppMemberFunctionMayBeStatic
// ReSharper disable CppUseAuto
#include <cstring>
#include "Raygine.h"

#include "DirectionalLight.h"
#include "Sphere.h"
#include "Tracer.h"
#include "ExternLibs/rapidxml-1.13/rapidxml.hpp"

int main()
{
	std::unique_ptr<RaygineTracer::Raygine> IRaygine = std::make_unique<RaygineTracer::Raygine>();
	IRaygine->Initialise();
	
	//std::thread TRaygine = std::thread(&RaygineTracer::Raygine::Initialise, IRaygine.get());
	//TRaygine.join(); // The main thread won't terminate until this thread terminates, so no need to do a while loop here.
}

/**
 * \brief Initialises Raygine.
 */
void RaygineTracer::Raygine::Initialise()
{
	IFileSystemManager = std::make_shared<FileSystemManager>();
	IXMLParser = std::make_shared<XMLParser>(IFileSystemManager);
	
	std::vector<std::unique_ptr<Tracer>> tracerPreThreads;

	rayHittables = std::make_shared<RayHittableList>();

	// If the settings can't be loaded, we can continue but just use the defaults defined for the Settings struct...
	if (LoadSettingsFile())
		std::cout << "Raygine - Successfully loaded settings from file.\n";
	else
		std::cout << "Raygine - Error, unable to load settings file. System defaults will be used.\n";

	//Vector3 lookFrom = Vector3(13, 2, 3);
	mainCamera = std::make_shared<Camera>(Vector3Zero, Vector3Forward, Vector3Up, 75.0f, GetTracerSettings().width / GetTracerSettings().height, 0.01f, 100.0f);

	// Start ray tracing stuff here.

	rayHittables->AddRayHittable(std::make_shared<Sphere>(Vector3(1.0f,0,3), 1.0f)); // Earth
	rayHittables->AddRayHittable(std::make_shared<Sphere>(Vector3(-1.5f, 1.0f, 4), 0.3f)); // Moon
	rayHittables->AddRayHittable(std::make_shared<DirectionalLight>(Vector3(1.5f, 0, -1), 0.3f)); // Sun

	std::string output;
	std::string outFilePath;
	std::stringstream sstream;

	sstream << GetTracerSettings().rendererOutDir << "\\out.ppm";
	outFilePath = sstream.str();
	sstream.str("");

	sstream << "P3\n" << GetTracerSettings().width << " " << GetTracerSettings().height << "\n255\n";
	
	const float cpuThreads = static_cast<float>(GetTracerSettings().cpuThreads);
	sharedWidth = static_cast<float>(GetTracerSettings().width);

	std::cout << "Raygine - > Benchmark. (incrementing threads).\n";
	for (int i = 1; i <= cpuThreads; i++)
	{
		std::cout << "Raygine - > Running benchmark number " << i << " of " << cpuThreads << " (" << i << " cores) ";

		tracerPreThreads.clear(); // This is safe because all existing threads have terminated, and in the first run this doesn't do anything.
		tracerThreads.clear();

		sharedHeight = static_cast<float>(GetTracerSettings().height) / i;
		
		DoRun(i, tracerPreThreads);
		std::cout << (endTime - startTime) / 1000.0f << "s\n";
	}
}

void RaygineTracer::Raygine::DoRun(const float& threadCount, std::vector<std::unique_ptr<Tracer>> &tracerPreThreads)
{
	for (unsigned int i = 0; i < threadCount; i++)
	{
		tracerPreThreads.emplace_back(std::make_unique<Tracer>(this, i));
		tracerThreads[i] =
			std::thread(
				&Tracer::Trace,
				tracerPreThreads[i].get(),
				sharedWidth,
				sharedHeight
			);
	}

	startTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

	int n = 1;
	for (auto &thread : tracerThreads)
	{
		thread.second.join();
	}

	endTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

/**
 * \brief Loads the Raygine settings file.
 * \returns A bool to signify if the file has been loaded correctly.
 */
const bool RaygineTracer::Raygine::LoadSettingsFile()
{
	std::shared_ptr<rapidxml::xml_document<>> const shaderList = IXMLParser->ParseXML("RaygineSettings.xml");

	if (shaderList != nullptr)
	{
		for (rapidxml::xml_node<>* node = shaderList->first_node(); node; node = node->next_sibling())
		{
			if (std::strcmp(node->name(), "settings") == 0)
			{
				for (rapidxml::xml_node<>* list = node->first_node(); list; list = list->next_sibling())
				{
					if (std::strcmp(list->name(), "output") == 0)
					{
						if (list->first_attribute("width") && list->first_attribute("height"))
						{
							tracerSettings.width = ConstCharToUInt(list->first_attribute("width")->value());
							tracerSettings.height = ConstCharToUInt(list->first_attribute("height")->value());

							std::cout << "Raygine - Configuration resolution set: " << tracerSettings.width << "x" << tracerSettings.height << "\n";
						}
					}
					else if (std::strcmp(list->name(), "cpusettings") == 0)
					{
						if (list->first_attribute("threads"))
						{
							tracerSettings.cpuThreads = ThreadsToUInt(list->first_attribute("threads")->value());
							std::cout << "Raygine - Configuration CPU thread count set: " << tracerSettings.cpuThreads << "\n";
						}
					}
					else if (std::strcmp(list->name(), "renderer") == 0)
					{
						for (rapidxml::xml_node<>* rendererNode = list->first_node(); rendererNode; rendererNode = rendererNode->next_sibling())
						{
							if (std::strcmp(rendererNode->name(), "outdir") == 0)
							{
								tracerSettings.rendererOutDir = rendererNode->value();
								std::cout << "Raygine - Configuration Renderer output directory set: " << IFileSystemManager->GetExecutableDirectory() << "\\" << tracerSettings.rendererOutDir << "\\\n";
							}
						}
					}
				}
			}
		}
		return true;
	}
	else
		return false;
}

/**
 * \brief Converts the given constant character array (const char*) to a uint. Warning: unsafe conversion, check the input value before use.
 * \returns An unsigned int.
 */
unsigned RaygineTracer::Raygine::ConstCharToUInt(const char* input) const
{
	unsigned int output;
	std::stringstream sstream(input);
	sstream >> output;

	return output;
}

/**
 * \brief Converts the given thread value from the settings XML file to a usable unsigned int.
 * \returns An unsigned int, representative of the number of threads the ray tracing needs to utilise. Output has already been checked and sanitised for invalid values.
 */
unsigned RaygineTracer::Raygine::ThreadsToUInt(const char* value) const
{
	const unsigned int numOfAvailableThreads = GetNumOfThreadsAvailable();
	const unsigned int requestedNumOfThreads = ConstCharToUInt(value);

	// Using strcmp we avoid having to cast to std::string.
	if (std::strcmp(value, "MAX") == 0)
		return numOfAvailableThreads;
	if (std::strcmp(value, "MIN") == 0)
		return 1;
	
	if (requestedNumOfThreads > numOfAvailableThreads)
		return numOfAvailableThreads;
	if (requestedNumOfThreads < 1)
		return 1;
		
	return requestedNumOfThreads;
}

/**
 * \brief Finds the number of physical CPU threads reported by the OS (inc. hyperthreaded threads).
 * \returns The number of physical threads reported by the OS.
 */
unsigned int RaygineTracer::Raygine::GetNumOfThreadsAvailable() const
{
	// NOTE: Due to OS scheduling we do not decide if our threads are executed by different physical CPU threads!
	const unsigned int threadCount = std::thread::hardware_concurrency();

	// Will return 0 if hardware_concurrency cannot find the number of threads the system has (per https://en.cppreference.com/w/cpp/thread/thread/hardware_concurrency).
	if (threadCount != 0)
		return threadCount;

	return 1;
}