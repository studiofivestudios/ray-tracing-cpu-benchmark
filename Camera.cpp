/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#include "Camera.h"

RaygineTracer::Camera::Camera(const Vector3& lookFrom, const Vector3& lookAt, const Vector3& up,
                              const float& verticalFov, const float& aspect, const float& aperture, const float& focusDistance)
{
	const float halfHeight = std::tanf(((verticalFov * 3.14) / 180) / 2.0f);
	const float halfWidth = aspect * halfHeight;

	w = Vector3::GetUnitVector(lookFrom - lookAt);
	u = Vector3::GetUnitVector(Vector3::GetCrossProduct(up, w));
	v = Vector3::GetCrossProduct(w, u);

	origin = lookFrom;
	lowerLeft = origin - halfWidth * focusDistance * u - halfHeight * focusDistance * v - focusDistance * w;
	horizontal = 2 * halfWidth * focusDistance * u;
	vertical = 2 * halfHeight * focusDistance * v;
	lensRadius = aperture / 2.0f;
}

std::shared_ptr<RaygineTracer::Mathematics::Ray> RaygineTracer::Camera::GetRay(const float& s, const float& time)
{
	Vector3 rd = lensRadius * RandomInUnitDisk();
	Vector3 offset = u * rd.x + v * rd.y;
	return std::make_shared<Mathematics::Ray>(origin + offset, lowerLeft + s * horizontal + time * vertical - origin - offset);
}

Vector3 RaygineTracer::Camera::RandomInUnitDisk()
{
	Vector3 p;
	const Vector3 oneOneZero = Vector3(1, 1, 0);

	while (Vector3::DotProduct(p,p) >= 1.0f)
		p = Vector3(RandomDouble(), RandomDouble(), 0) * 2.0 - oneOneZero;

	return p;
}