/*
 * UWE AT Task 3 Ray Tracing "Raygine".
 * 17025080 Alexander Stopher alexander2.stopher@live.uwe.ac.uk
 *
 * Description: Bounding box class (aka, BVH). Code File.
 */

#include "BoundingBox.h"
