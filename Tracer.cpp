/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

// ReSharper disable CppJoinDeclarationAndAssignment
#include "Tracer.h"

#include "RayHittableList.h"

/**
 * \brief Performs the requested ray tracing.
 * \param _width
 * \param _height
 */
void RaygineTracer::Tracer::Trace(const float& _width, const float& _height)
{
	const Raygine::Settings tracerSettings = IRaygine->GetTracerSettings();
	Camera* camera = IRaygine->GetCamera();
	
	const float verticalOffset = _height * (tracerNumber + 1); // Apply an offset to make the ray behave as if we're doing this in one big image instead of multithreading!
	float horizontal, vertical;
	const std::shared_ptr<RayHittableList> rayHittables = IRaygine->GetRayHittables();
	std::shared_ptr<Ray> ray;

	startTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

	for (float height = _height; height > 0; height--)
	{
		for (float width = 0; width < _width; width++)
		{
			horizontal = width / tracerSettings.width;
			vertical = (verticalOffset + height) / tracerSettings.height;

			//std::shared_ptr<Ray> ray = camera->GetRay(vertical + RandomDouble() / _width, horizontal + RandomDouble() / _height);
			std::shared_ptr<Ray> ray = std::make_shared<Ray>(camera->GetOrigin(), camera->GetLowerLeft() + horizontal * camera->GetHorizontal() + vertical * camera->GetVertical());
			
			resultStorage.push_back(Colour::DoColour(ray, rayHittables));
		}
	}

	endTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	isDone = true;
}