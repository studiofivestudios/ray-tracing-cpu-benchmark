/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef RAYGINE_H
#define RAYGINE_H

// ReSharper disable once CppUnusedIncludeDirective
#include <iostream> // ReSharper doesn't understand that we have the entry point outside of the class in the code file, so gives an unused include warning.
#include <thread>
#include <unordered_map>

#include "Camera.h"
#include "Common/FileSystem.h"
#include "Common/XMLParser.h"

#include "RayHittable.h"
#include "RayHittableList.h"

namespace RaygineTracer
{
	class Tracer;
	using namespace Common;
	using namespace Common::Mathematics;
	using namespace Mathematics;
	
	class Raygine
	{
	public:
		struct Settings
		{
			unsigned int width			= 1920;		// Numeric non-negative value, no maximum. Minimum 1.
			unsigned int height			= 1080;		// Numeric non-negative value, no maximum. Minimum 1.
			int cpuThreads				= 1;		// MAX (max threads available on system), MIN (one thread), or numeric value.
			const char* rendererOutDir	= "Output"; // Constant character array, file path relative to the working directory.
		};
		
		Raygine() = default;
		~Raygine() = default;

		void Initialise();

		[[nodiscard]] Settings GetTracerSettings() const { return tracerSettings; }
		[[nodiscard]] Camera* GetCamera() const { return mainCamera.get(); }
		[[nodiscard]] std::shared_ptr<RayHittableList> GetRayHittables() const { return rayHittables; }

	private:
		std::shared_ptr<XMLParser> IXMLParser = nullptr;
		std::shared_ptr<FileSystemManager> IFileSystemManager = nullptr;

		std::shared_ptr<Camera> mainCamera = nullptr;

		Settings tracerSettings;

		bool exiting = false;

		float sharedHeight = 0;
		float sharedWidth = 0;

		std::unordered_map<unsigned int, std::thread> tracerThreads;
		std::shared_ptr<RayHittableList> rayHittables;

		long long startTime;
		long long endTime;

		[[nodiscard]] const bool LoadSettingsFile();
		[[nodiscard]] unsigned int ConstCharToUInt(const char* input) const;
		[[nodiscard]] unsigned int ThreadsToUInt(const char* value) const;
		[[nodiscard]] unsigned int GetNumOfThreadsAvailable() const;

		void DoRun(const float &threadCount, std::vector<std::unique_ptr<Tracer>> &tracerPreThreads);
	};
}
#endif // RAYGINE_H