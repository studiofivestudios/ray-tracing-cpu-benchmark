/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef HITTABLE_H
#define HITTABLE_H

#include "Ray.h"

namespace RaygineTracer::Mathematics
{
	using namespace Common::Mathematics;
	
	class HitTable
	{
	public:
		virtual ~HitTable() = default;

		struct HitRecord
		{
			float t;
			Vector3 p;
			Vector3 normal;
		};

		virtual bool Hit(const Ray& ray, const float& tMin, const float& tMax, const HitRecord& rec) const = 0;
	};
}

#endif // HITTABLE_H
