/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef COLOUR_H
#define COLOUR_H

#include "RayHittable.h"
#include "RayHittableList.h"

namespace RaygineTracer::Mathematics
{
	class Colour : Vector3
	{
	public:
		explicit Colour() = default;
		Colour(float _r, float _g, float _b) : Vector3(_r, _g, _b) { r = _r; g = _g; b = _b; }
		Colour(const Vector3& vec): Vector3(vec)
		{
			r = vec.x;
			g = vec.y;
			b = vec.z;
		}

		float r = 0.0f;
		float g = 0.0f;
		float b = 0.0f;

		[[nodiscard]] static Colour DoColour(std::shared_ptr<Ray> ray, const std::shared_ptr<RayHittableList> world);
	};

	inline Colour ColourBlack{ 0,0,0 };
}
#endif // COLOUR_H
