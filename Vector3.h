/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef VECTOR3_H
#define VECTOR3_H

// ReSharper disable once CppUnusedIncludeDirective
#include <cmath>

namespace Common::Mathematics {
	class Vector3
	{
	public:
		float x = 0.0f;
		float y = 0.0f;
		float z = 0.0f;

		explicit Vector3() = default;
		explicit Vector3(const float &_x, const float & _y, const float & _z) : x(_x), y(_y), z(_z) { }

		[[nodiscard]] Vector3 operator +(const Vector3 vec) const
		{
			return Vector3{ x + vec.x, y + vec.y, z + vec.z };
		}

		[[nodiscard]] Vector3 operator -(const Vector3 vec) const
		{
			return Vector3{ x - vec.x, y - vec.y, z - vec.z };
		}

		[[nodiscard]] Vector3 operator *(const Vector3 vec) const
		{
			return Vector3{ x * vec.x, y * vec.y, z * vec.z };
		}

		[[nodiscard]] Vector3 operator *(const float what) const
		{
			return Vector3{ x * what, y * what, z * what };
		}

		[[nodiscard]] Vector3 operator /(const Vector3 vec) const
		{
			return Vector3{ x / vec.x, y / vec.y, z / vec.z };
		}

		[[nodiscard]] Vector3 operator/(float n) const
		{
			return Vector3{ x / n, y / n, z / n };
		}

		[[nodiscard]] Vector3 operator-(float n) const
		{
			return Vector3{ x - n, y - n, z - n };
		}

		[[nodiscard]] Vector3 operator+(const int &n) const
		{
			return Vector3{ x + n, y + n, z + n };
		}

		[[nodiscard]] float GetLength() const;
		[[nodiscard]] float GetLengthSquared() const;


		[[nodiscard]] static const bool Equal(const Vector3& a, const Vector3& b)
		{
			return (a.x == b.x && a.y == b.y && a.z == b.z);
		}
		
		[[nodiscard]] static Vector3 GetUnitVector(Vector3 vector);
		[[nodiscard]] static Vector3 GetCrossProduct(const Vector3& a, const Vector3& b);

		[[nodiscard]] static float DotProduct(const Vector3& a, const Vector3& b);
	};

	static const Vector3 Vector3Forward			= Vector3{	0,	0,	1	};
	static const Vector3 Vector3Backward		= Vector3{	0,	0,	-1	};
	static const Vector3 Vector3Left			= Vector3{	-1,	0,	0	};
	static const Vector3 Vector3Right			= Vector3{	1,	0,	0	};
	static const Vector3 Vector3Up				= Vector3{	0,	1,	0	};
	static const Vector3 Vector3Down			= Vector3{	0,	-1,	0	};
	static const Vector3 Vector3Zero			= Vector3{	0,	0,	0	};
	static const Vector3 Vector3One				= Vector3{	1,	1,	1	};

	[[nodiscard]] inline Vector3 operator*(const float& lhs, const Vector3& vec)
	{
		return Vector3{ lhs * vec.x, lhs * vec.y, lhs * vec.z };
	}

	[[nodiscard]] inline Vector3 operator-(const float &lhs, const Common::Mathematics::Vector3& vec)
	{
		return Vector3{ vec.x - lhs, vec.y - lhs, vec.z - lhs };
	}
}
#endif