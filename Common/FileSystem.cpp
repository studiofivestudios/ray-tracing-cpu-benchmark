/*
 * UWE AT Task 3 Ray Tracing "Raygine".
 * 17025080 Alexander Stopher alexander2.stopher@live.uwe.ac.uk
 *
 * Description: File system manager (Code File)
 *
 * NOTE: class is from AT Task 1, with some modifications (such as better documentation).
 */

#include "FileSystem.h"

/**
* \brief Loads the given file and returns its File instance.
* \param fileName The file name to load. Is relative to current working directory.
* \param keepOpen States if we need to keep the file open (e.g. for log files that are constantly written to). This locks the file until closed.
* \param relativeToGame If the given directory is relative to the Game folder, outside of current working directory.
* \return An instance of File, representing the file.
*/
Common::File* Common::FileSystemManager::LoadFile(const char* fileName, const bool &keepOpen, const bool &relativeToGame) {
	char newPath[MAX_PATH] = "";
	PathCombine(newPath, GetExecutableDirectory().c_str(), fileName);

	if (!IsFileAlreadyLoaded(newPath)) {
		files.push_back(std::make_unique<File>(newPath, keepOpen));

		return files[files.size() - 1].get();
	}

	return GetFile(fileName);
}

/**
 * \brief Get the File instance for the specified file.
 * \param fileName The file name to retrieve the instance for.
 * \return The instance of the file, or nullptr if it does not exist.
 */
Common::File* Common::FileSystemManager::GetFile(const char* fileName)
{
	for (std::unique_ptr<File>& file : files) {
		if (file.get()->GetName() == fileName)
			return file.get();
	}

	return nullptr;
}

/**
 * \brief Loads the given file and returns its contents.
 * \param fileName The file name to load. Is relative to current working directory.
 * \param keepOpen States if we need to keep the file open (e.g. for log files that are constantly written to). This locks the file until closed.
 * \param relativeToGame If the given directory is relative to the Game folder, outside of current working directory.
 * \return A character array containing the file's contents.
 */
char* Common::FileSystemManager::LoadAndGetContents(const char* fileName, const bool& keepOpen, const bool& relativeToGame) {
	File* file = LoadFile(fileName, keepOpen, relativeToGame);

	if (file != nullptr)
	{
		const char *_fileContents = file->GetContents();
		return const_cast<char*>(_fileContents);
	}

	return nullptr;
}

/**
 * \brief Writes the given character array (string) to file.
 * \param fileName The file name to write to. Will overwrite the file if it already exists. Is relative to current working directory.
 * \param fileContents The character array to write.
 * \return Whether the operation has succeeded or failed.
 */
bool Common::FileSystemManager::WriteFileS(const char* fileName, const std::string &fileContents)
{
	File* file = LoadFile(fileName, true, false);
	
	if (file != nullptr)
	{
		std::ofstream fstream;
		fstream.open(file->GetName());

		fstream << fileContents;
		fstream.close();
		
		// TODO: FIXME
		//file->Write(fileContents, false);
		return true;
	}
	
	return false;
}

/**
 * \brief Writes the given byte array to file.
 * \param fileName The file name to write to. Will overwrite the file if it already exists. Is relative to current working directory.
 * \param fileContents The byte array to write.
 * \return Whether the operation has succeeded or failed.
 */
bool Common::FileSystemManager::WriteFile(const char* fileName, const byte fileContents[])
{
	return false;
}

/**
 * \brief Checks if the given file has already been loaded and is still open.
 * \param fileName The file name to check. Is relative to current working directory.
 * \return Whether the operation has succeeded or failed.
 */
bool Common::FileSystemManager::IsFileAlreadyLoaded(const char* fileName) {
	for (std::unique_ptr<File> &file : files) {
		if (file.get()->GetName() == fileName)
			return true;
	}

	return false;
}

/**
 * \brief Retrieves the file names within a directory.
 * \param directory The directory name to check. Is relative to current working directory.
 * \param relativeToGame If the given directory is relative to the Game folder, outside of current working directory.
 * \return A vector of string containing all file names within the given directory.
 */
std::vector<std::string> Common::FileSystemManager::EnumerateDirectory(const char* directory, const bool &relativeToGame)
{
	std::vector<std::string> results;
	char newPath[MAX_PATH] = "";

	if (relativeToGame)
		PathCombine(newPath, GetExecutableDirectory().c_str(), directory);

	if (std::filesystem::exists(newPath))
	{
		for (const auto& entry : std::filesystem::directory_iterator(newPath))
			results.push_back(entry.path().string());
	}

	return results;
}

//https://gist.github.com/karolisjan/f9b8ac3ae2d41ec0ce70f2feac6bdfaf
/**
 * \brief Find the current working directory.
 * \return A string containing the full current working directory.
 */
std::string Common::FileSystemManager::GetExecutableDirectory()
{
	char buffer[MAX_PATH];
	GetModuleFileNameA(nullptr, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");

	return std::string(buffer).substr(0, pos);
}