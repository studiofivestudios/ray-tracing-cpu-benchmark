/*
 * UWE AT Task 3 Ray Tracing "Raygine".
 * 17025080 Alexander Stopher alexander2.stopher@live.uwe.ac.uk
 *
 * Description: File system manager (Header)
 *
 * NOTE: class is from AT Task 1.
 */

#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <filesystem>
#include <mutex>
#include <shlwapi.h>
#include <vector>

#include "File.h" 

namespace Common {
	class FileSystemManager
	{
	public:
		FileSystemManager() = default;
		~FileSystemManager() = default;

		[[nodiscard]] File* LoadFile(const char* fileName, const bool& keepOpen, const bool& relativeToGame = true);
		[[nodiscard]] File* GetFile(const char* fileName);
		
		[[nodiscard]] char* LoadAndGetContents(const char* fileName, const bool& keepOpen, const bool& relativeToGame = true);

		[[nodiscard]] bool WriteFileS(const char* fileName, const std::string &fileContents);
		[[nodiscard]] bool WriteFile(const char* fileName, const byte fileContents[]);
		[[nodiscard]] bool IsFileAlreadyLoaded(const char* fileName);

		[[nodiscard]] std::string GetExecutableDirectory();

		[[nodiscard]] std::vector<std::string> EnumerateDirectory(const char* directory, const bool& relativeToGame = true);
	private:
		std::mutex lock;
		std::vector<std::unique_ptr<File>> files;
	};
}

#endif