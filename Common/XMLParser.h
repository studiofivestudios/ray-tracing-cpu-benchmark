/*
 * UWE AT Task 3 Ray Tracing "Raygine".
 * 17025080 Alexander Stopher alexander2.stopher@live.uwe.ac.uk
 *
 * Description: XML Parser (Header)
 *
 * NOTE: class is from AT Task 1.
 */

#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <string>

#include "ExternLibs/rapidxml-1.13/rapidxml.hpp"

#include "FileSystem.h"
//#include "..\Raygine.h"

namespace Common
{
    class XMLParser
    {
    public:
        XMLParser(std::shared_ptr<FileSystemManager> _IFileSystemManager) : IFileSystemManager(_IFileSystemManager) { }
        ~XMLParser() = default;

        std::shared_ptr<rapidxml::xml_document<>> ParseXML(const std::string& path) const;

    private:
        std::shared_ptr<FileSystemManager> IFileSystemManager = nullptr;
    };
}
#endif