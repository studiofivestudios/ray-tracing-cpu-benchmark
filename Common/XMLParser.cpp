/*
 * UWE AT Task 3 Ray Tracing "Raygine".
 * 17025080 Alexander Stopher alexander2.stopher@live.uwe.ac.uk
 *
 * Description: XML Parser (Code File)
 *
 * NOTE: class is from AT Task 1.
 */

#include "XMLParser.h"

//#include "../ResourceLocator_.h"

std::shared_ptr<rapidxml::xml_document<>> Common::XMLParser::ParseXML(const std::string& path) const
{
    std::shared_ptr<rapidxml::xml_document<>> doc = std::make_shared<rapidxml::xml_document<>>();

    const auto file_buffer = IFileSystemManager->LoadAndGetContents(path.c_str(), false);

    if (file_buffer != nullptr)
    {
        doc.get()->parse<rapidxml::parse_no_entity_translation>(file_buffer);
        return doc;
    }

    return nullptr;
}
