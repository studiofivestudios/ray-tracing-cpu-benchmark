/*
 * UWE AT Task 3 Ray Tracing "Raygine".
 * 17025080 Alexander Stopher alexander2.stopher@live.uwe.ac.uk
 *
 * Description: File system manager, file class (Code File)
 *
 * NOTE: class is from AT Task 1.
 */

#ifndef FILE_H
#define FILE_H

#include <fstream>
#include <sstream>
#include <iostream>

namespace Common {
	class File
	{
	public:
		File(const char* _fileName, const bool &keepOpen = false);
		~File() = default;

		const char* GetContents() { return fileContents.c_str(); };
		std::string GetName() { return fileName; }
		bool GetIsClosedOnDisk() const;

		void Close() { if (!GetIsClosedOnDisk()) fileHandle.close(); };
		void Open(const bool& appendMode = false);
		void Reload(const bool& appendMode);
		void Write(const char* contents, const bool &append = false);
	private:
		std::string fileName;
		std::string fileContents;

		std::fstream fileHandle;
	};
}

#endif