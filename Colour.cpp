/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#include "Colour.h"

RaygineTracer::Mathematics::Colour RaygineTracer::Mathematics::Colour::DoColour(std::shared_ptr<Ray> ray,
	const std::shared_ptr<RayHittableList> world)
{
	RayHittable::HitRecord hitRecord;
	if (world->Hit(ray, 0.001, FLT_MAX, hitRecord))
		return Colour(0.5f * Colour(hitRecord.normal.x + 1, hitRecord.normal.y + 1, hitRecord.normal.z + 1));

	return ColourBlack;
}