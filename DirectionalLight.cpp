/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#include "DirectionalLight.h"

bool RaygineTracer::Mathematics::DirectionalLight::Hit(std::shared_ptr<Ray> ray, const float& tMin, const float& tMax, HitRecord& rec)
{
	Vector3 oc = ray->GetOrigin() - emittedFrom;
	const float a = Vector3::DotProduct(ray->GetDirection(), ray->GetDirection());
	const float b = Vector3::DotProduct(oc, ray->GetDirection());
	const float c = Vector3::DotProduct(oc, oc) - radius * radius;
	const float discriminant = (b * b) - (a * c);

	if (discriminant > 0)
	{
		float tmp = (-b - std::sqrt(b * b - a * c)) / a;

		if (tmp < tMax && tmp > tMin)
		{
			rec.t = tmp;
			rec.p = ray->PointAt(rec.t);
			rec.normal = (rec.p - emittedFrom) / radius;

			return true;
		}

		tmp = (-b + std::sqrt(b * b - a * c)) / a;

		if (tmp < tMax && tmp > tMin)
		{
			rec.t = tmp;
			rec.p = ray->PointAt(rec.t);
			rec.normal = (rec.p - emittedFrom) / radius;

			return true;
		}
	}

	return false;
}

std::shared_ptr<RaygineTracer::Mathematics::AxisAlignedBoundingBox> RaygineTracer::Mathematics::DirectionalLight::GetBoundingBox(const float& t0, const float& t2) const
{
	return std::make_shared<AxisAlignedBoundingBox>(emittedFrom - Vector3(radius, radius, radius), emittedFrom + Vector3(radius, radius, radius));
}
