/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

// ReSharper disable CppClangTidyModernizePassByValue
#include "RayHittableList.h"

bool RaygineTracer::Mathematics::RayHittableList::Hit(const std::shared_ptr<Ray> ray, const float& tMin, const float& tMax,
	HitRecord& rec)
{
	HitRecord tmpRecord;
	bool hasHitAnything = false;
	float closest = tMax;

	for (auto &hittable : rayHittableList)
	{
		if (hittable->Hit(ray, tMin, closest, tmpRecord))
		{
			hasHitAnything = true;
			closest = tmpRecord.t;
			rec = tmpRecord;
		}
	}

	return hasHitAnything;
}

std::shared_ptr<RaygineTracer::Mathematics::AxisAlignedBoundingBox> RaygineTracer::Mathematics::RayHittableList::
GetBoundingBox(const float& t0, const float& t1) const
{
	if (rayHittableList.empty())
		return nullptr;

	std::shared_ptr<AxisAlignedBoundingBox> tmpBox = rayHittableList[0]->GetBoundingBox(t0, t1);
	std::shared_ptr<AxisAlignedBoundingBox> tmpBox2;

	if (tmpBox != nullptr)
	{
		for (auto hittable : rayHittableList)
		{
			tmpBox2 = hittable->GetBoundingBox(t0, t1);
			if (tmpBox2 != nullptr)
				tmpBox = GetSurroundingBox(tmpBox, tmpBox2);
		}
	}

	return nullptr;
}