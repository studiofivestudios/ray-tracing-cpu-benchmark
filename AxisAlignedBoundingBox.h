/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef AXISALIGNEDBOUNDINGBOX_H
#define AXISALIGNEDBOUNDINGBOX_H

#include <memory>

#include "Vector3.h"

namespace RaygineTracer::Mathematics
{
	using namespace Common::Mathematics;
	class Ray;

	class AxisAlignedBoundingBox
	{
	public:
		AxisAlignedBoundingBox(const Vector3& a, const Vector3& b);

		[[nodiscard]] Vector3 GetMin() const { return min; }
		[[nodiscard]] Vector3 GetMax() const { return max; }

		void GetAxisValue(const unsigned int &axis, const Vector3& vec, float &axisValue) const;

		[[nodiscard]] static float FMin(const float& a, const float& b);
		[[nodiscard]] static float FMax(const float& a, const float& b);
		
		[[nodiscard]] bool Hit(std::shared_ptr<Ray> ray, const float& tMin, const float& tMax) const;
	private:
		Vector3 min, max;
	};
}

#endif // AXISALIGNEDBOUNDINGBOX_H