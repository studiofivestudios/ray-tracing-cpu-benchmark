/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef DIRECTIONALLIGHT_H
#define DIRECTIONALLIGHT_H
#include "RayHittable.h"

namespace RaygineTracer::Mathematics
{
	class DirectionalLight :
		public RayHittable
	{
	public:
		explicit DirectionalLight(const Vector3& _emittingFrom, const float& _radius) : emittedFrom(_emittingFrom), radius(_radius) { }
		virtual ~DirectionalLight() = default;

		[[nodiscard]] bool Hit(std::shared_ptr<Ray>, const float& tMin, const float& tMax, HitRecord& rec) override;

		[[nodiscard]] std::shared_ptr<AxisAlignedBoundingBox> GetBoundingBox(const float& t0, const float& t2) const override;
	private:
		const Vector3 emittedFrom;
		const float radius;
	};
}
#endif // DIRECTIONALLIGHT_H
