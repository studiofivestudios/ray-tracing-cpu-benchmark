/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef CAMERA_H
#define CAMERA_H

#include <memory>

#include "Ray.h"
#include "Vector3.h"

namespace RaygineTracer
{
	using namespace Common::Mathematics;
	
	class Camera
	{
	public:
		explicit Camera(const Vector3& lookFrom, const Vector3& lookAt, const Vector3& up, const float& verticalFov, const float& aspect, const float& aperture, const float& focusDistance);

		[[nodiscard]] Vector3 GetLowerLeft() const { return lowerLeft; }
		[[nodiscard]] Vector3 GetHorizontal() const { return horizontal; }
		[[nodiscard]] Vector3 GetVertical() const { return vertical; }
		[[nodiscard]] Vector3 GetOrigin() const { return origin; }

		[[nodiscard]] std::shared_ptr<Mathematics::Ray> GetRay(const float& s, const float& time);
	private:
		Vector3 lowerLeft, horizontal, vertical, origin, u, v, w;
		float lensRadius;

		[[nodiscard]] Vector3 RandomInUnitDisk();
	};

	// ReSharper disable once CppMemberFunctionMayBeStatic
	[[nodiscard]] static double RandomDouble() {
		return rand() / (RAND_MAX + 1.0);
	}
}
#endif // CAMERA_H