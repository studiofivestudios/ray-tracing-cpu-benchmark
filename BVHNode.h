/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef BVHNODE_H
#define BVHNODE_H
#include <memory>
#include <vector>

#include "RayHittable.h"

namespace RaygineTracer::Mathematics
{
	// ReSharper disable once CppInconsistentNaming
	class BVHNode final : public RayHittable
	{
	public:
		virtual ~BVHNode() = default;

		struct BVHTree
		{
			std::shared_ptr<RayHittable> left;
			std::shared_ptr<RayHittable> right;
		};

		class BoxCompare
		{
		public:
			Vector3 axis;
			[[nodiscard]] const int operator()(std::shared_ptr<RayHittable> a, std::shared_ptr<RayHittable> b) const;
		};

		BVHNode(const std::vector<std::shared_ptr<RayHittable>> &rayHittableList, const int &n, const float& t0, const float& t1);
		
		/**
		 * \brief Retrieves the left and right branches for this node.
		 * \return An instance of BVHTree, containing pointers to the left and right objects in this node.
		 */
		[[nodiscard]] BVHTree GetNodeChildren() const { return nodeBranches; }

		[[nodiscard]] bool Hit(std::shared_ptr<Ray> ray, const float& tMin, const float& tMax, HitRecord& rec) override;
		[[nodiscard]] std::shared_ptr<AxisAlignedBoundingBox> GetBoundingBox(const float& t0, const float& t1) const override;

		std::shared_ptr<AxisAlignedBoundingBox> boundingBox;
	private:
		BVHTree nodeBranches;

		static inline const Vector3 directions[3] = { Vector3Right, Vector3Up, Vector3Forward };
	};
}
#endif // BVHNODE_H