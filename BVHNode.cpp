/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */
#include <algorithm>
#include <iostream>

#include "BVHNode.h"

RaygineTracer::Mathematics::BVHNode::BVHNode(const std::vector<std::shared_ptr<RayHittable>>& rayHittableList,
                                             const int& n, const float& t0, const float& t1)
{
	// Is there a more efficient way of doing this?
	Vector3 axis = directions[rand() % 3];
	std::vector<std::shared_ptr<RayHittable>> rayHittableListCpy = rayHittableList; // std::sort doesn't like the vector being const.

	// Could this cause problems for multithreading?
	std::sort(rayHittableListCpy.begin(), rayHittableListCpy.end(), BoxCompare(axis));

	if (n == 1)
		nodeBranches.left = nodeBranches.right = rayHittableList[0];
	else if (n == 2)
	{
		nodeBranches.left = rayHittableList[0];
		nodeBranches.right = rayHittableList[1];
	}
	else
	{
		nodeBranches.left = std::make_shared<BVHNode>(rayHittableList, n / 2, t0, t1);
		nodeBranches.right = std::make_shared<BVHNode>(rayHittableList, n - n / 2, t0, t1);
	}

	const std::shared_ptr<AxisAlignedBoundingBox> boxLeft = nodeBranches.left->GetBoundingBox(0, 0);
	const std::shared_ptr<AxisAlignedBoundingBox> boxRight = nodeBranches.right->GetBoundingBox(0, 0);

	if (boxLeft != nullptr || boxRight != nullptr)
		boundingBox = GetSurroundingBox(boxLeft, boxRight);
}

const int RaygineTracer::Mathematics::BVHNode::BoxCompare::operator()(std::shared_ptr<RayHittable> a,
	std::shared_ptr<RayHittable> b) const
{
	const std::shared_ptr<AxisAlignedBoundingBox> boxLeft = a->GetBoundingBox(0, 0);
	const std::shared_ptr<AxisAlignedBoundingBox> boxRight = b->GetBoundingBox(0, 0);
	
	if (boxLeft != nullptr || boxRight != nullptr)
	{
		if (Vector3::Equal(axis, Vector3Right))
			return (boxLeft->GetMin().x - boxRight->GetMin().x) < .0f ? -1 : 1;
		if (Vector3::Equal(axis, Vector3Up))
			return (boxLeft->GetMin().y - boxRight->GetMin().y) < .0f ? -1 : 1;
		if (Vector3::Equal(axis, Vector3Forward))
			return (boxLeft->GetMin().z - boxRight->GetMin().z) < .0f ? -1 : 1;
	}
	
	return -1;
}

bool RaygineTracer::Mathematics::BVHNode::Hit(std::shared_ptr<Ray> ray, const float& tMin, const float& tMax, HitRecord& rec)
{
	if (boundingBox->Hit(ray, tMin, tMax))
	{
		HitRecord leftRecord, rightRecord;
		bool hitLeft = false, hitRight = false;

		// Check if each leaf generates a hit.
		if (nodeBranches.left != nullptr)
			hitLeft = nodeBranches.left->Hit(ray, tMin, tMax, leftRecord);
		if (nodeBranches.right != nullptr)
			hitRight = nodeBranches.right->Hit(ray, tMin, tMax, rightRecord);

		// If a hit is generated on either leaf...
		if (hitLeft || hitRight)
		{
			// If both branches are valid, then...
			if (hitLeft && hitRight)
			{
				if (leftRecord.t < rightRecord.t)
					rec = leftRecord;
				else
					rec = rightRecord;
			}
			else if (hitLeft)
				rec = leftRecord;
			else
				rec = rightRecord;

			return true;
		}
	}

	// No hits are detected.
	return false;
}

std::shared_ptr<RaygineTracer::Mathematics::AxisAlignedBoundingBox> RaygineTracer::Mathematics::BVHNode::GetBoundingBox(
	const float& t0, const float& t1) const
{
	return boundingBox;
}