/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

// ReSharper disable CppMemberFunctionMayBeStatic
// ReSharper disable CppJoinDeclarationAndAssignment

#include <memory>

#include "Ray.h"
#include "AxisAlignedBoundingBox.h"


RaygineTracer::Mathematics::AxisAlignedBoundingBox::AxisAlignedBoundingBox(const Vector3& a, const Vector3& b) : min(a), max(b)
{
}

void RaygineTracer::Mathematics::AxisAlignedBoundingBox::GetAxisValue(const unsigned int& axis, const Vector3 &vec, float& axisValue) const
{
	// ReSharper disable once CppDefaultCaseNotHandledInSwitchStatement
	switch (axis)  // NOLINT(hicpp-multiway-paths-covered)
	{
	case(0):
		axisValue = vec.x;
		break;
	case(1):
		axisValue = vec.y;
		break;
	case(2):
		axisValue = vec.z;
		break;
	}
}

bool RaygineTracer::Mathematics::AxisAlignedBoundingBox::Hit(std::shared_ptr<Ray> ray, const float& tMin, const float& tMax) const
{
	// https://medium.com/@bromanz/another-view-on-the-classic-ray-aabb-intersection-algorithm-for-bvh-traversal-41125138b525
	// Ray Tracing for the Rest Of Your Life (for the ffmin/ffmax).

	float _tMin, _tMax, axisValueMin, axisValueMax, rayOriginValue, rayDirectionValue, a, b;
	const Vector3 rayOrigin = ray->GetOrigin(), rayDirection = ray->GetDirection();

	for (unsigned int axis = 0; axis < 3; axis++) // Do this for all axis (X, Y, Z).
	{
		GetAxisValue(axis, min, axisValueMin);
		GetAxisValue(axis, max, axisValueMax);
		GetAxisValue(axis, rayOrigin, rayOriginValue);
		GetAxisValue(axis, rayDirection, rayDirectionValue);

		a = (axisValueMin - rayOriginValue) / rayDirectionValue;
		b = (axisValueMax - rayOriginValue) / rayDirectionValue;
		
		_tMin = FMax(a, b);
		_tMax = FMin(a, b);

		if ((FMax(_tMin, tMin) <= FMin(_tMax, tMax)))
			return false;
	}

	return true;
}

float RaygineTracer::Mathematics::AxisAlignedBoundingBox::FMin(const float& a, const float& b)
{
	return a < b ? a : b;
}

float RaygineTracer::Mathematics::AxisAlignedBoundingBox::FMax(const float& a, const float& b)
{
	return a > b ? a : b;
}
