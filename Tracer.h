/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef TRACER_H
#define TRACER_H

#include "Raygine.h"
#include "Colour.h"

namespace RaygineTracer
{
	class Tracer
	{
	public:
		explicit Tracer(Raygine* raygine, unsigned int &tracerN) : IRaygine(raygine), tracerNumber(tracerN) { };

		void Trace(const float& _width, const float& height);

		[[nodiscard]] std::vector<Colour> GetData() const { return resultStorage; }
		[[nodiscard]] bool GetIsDone() const { return isDone; }
		[[nodiscard]] long long GetCompletionTime() const { return startTime - endTime; }
	private:
		Raygine* IRaygine = nullptr;
		const unsigned int tracerNumber;

		std::vector<Colour>resultStorage;

		long long startTime;
		long long endTime;

		bool isDone = false;
	};
}
#endif // TRACER_H
