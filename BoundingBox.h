/*
 * UWE AT Task 3 Ray Tracing "Raygine".
 * 17025080 Alexander Stopher alexander2.stopher@live.uwe.ac.uk
 *
 * Description: Bounding box class (aka, BVH). Header.
 */

#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

namespace Raygine::Mathematics
{
	class BoundingBox
	{
	};
}

#endif // BOUNDINGBOX_H
