/*
 * (c)2021 Alexander Stopher, All Rights Reserved.
 */

#ifndef RAYHITTABLE_H
#define RAYHITTABLE_H

#include <memory>

#include "AxisAlignedBoundingBox.h"
#include "Ray.h"

namespace RaygineTracer::Mathematics
{
	class RayHittable
	{
	protected:
		RayHittable() = default;
		~RayHittable() = default;
	public:
		class HitRecord
		{
		public:
			float t = 0;
			Vector3 p = Vector3Zero;
			Vector3 normal = Vector3Zero;
		};

		[[nodiscard]] virtual bool Hit(std::shared_ptr<Ray> ray, const float& tMin, const float& tMax, HitRecord& rec) = 0;
		[[nodiscard]] virtual std::shared_ptr<AxisAlignedBoundingBox> GetBoundingBox(const float &t0, const float& t1) const = 0;
		[[nodiscard]] virtual std::shared_ptr<AxisAlignedBoundingBox> GetSurroundingBox(std::shared_ptr<AxisAlignedBoundingBox> box0, std::shared_ptr<AxisAlignedBoundingBox> box1) const;
	};

}
#endif // RAYHITTABLE_H
